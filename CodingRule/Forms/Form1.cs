﻿using CodingRule.Const;
using CodingRule.Util;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;

namespace CodingRule.Forms
{
    public partial class Form1 : Form
    {
        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public Form1()
        {
            InitializeComponent();

            // コントロール背景色の初期化
            ContorolUtil.SetContorolsColor(this);
        }
        #endregion

        /// <summary>
        /// 最大Byte数（textBox1）
        /// </summary>
        private const int _MAX_BYTE_TEXTBOX1 = 5;

        /// <summary>
        /// 文字列のByte数取得
        /// </summary>
        /// <param name="s">文字列</param>
        /// <returns>Byte数</returns>
        private int GetByte(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                // 文字列が空文字またはnullの場合、0を返す
                return 0;
            }
            // 文字列のByte数を返す
            Encoding enc = Encoding.GetEncoding("Shift_JIS");
            return enc.GetBytes(s).Length;
        }

        /// <summary>
        /// チェック処理（tesxtBox1）
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox1_Validate(object sender, CancelEventArgs e)
        {
            if (!this.JudgeExecuteValidate(sender))
            {
                // 後続のイベントをキャンセル
                return;

            }
            if (_MAX_BYTE_TEXTBOX1 < this.GetByte(this.textBox1.Text))
            {
                // textBox1の文字が最大Byte数より大きい場合、エラーメッセージ表示
                string message = string.Format(Messages._MSG_OVER_MAX_BYTE, this.textBox1.Name, _MAX_BYTE_TEXTBOX1);
                MessageBox.Show(message, Messages._CAP_ERROR);

                // 後続のイベントをキャンセル
                e.Cancel = true;
                return;
            }
        }

        /// <summary>
        /// Validate実行判定
        /// </summary>
        /// <param name="sender">イベントを送信したオブジェクト</param>
        /// <returns>判定フラグ</returns>
        private bool JudgeExecuteValidate(object sender)
        {
            if (this.btnClear.Focused)
            {
                // クリアボタンフォーカス時はValidateしない
                return false;
            }
            if (this.btnEnd.Focused)
            {
                // 終了ボタンフォーカス時はValidateしない
                return false;
            }
            if (this.ActiveControl == (Control)sender)
            {
                // アクティブコントロールがtextBox1ならValidateしない
                return false;
            }

            return true;
        }

        #region イベント
        /// <summary>
        /// クリアボタン押下イベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClear_Click(object sender, System.EventArgs e)
        {
            this.textBox1.Text = string.Empty;
            this.textBox1.Focus();
        }
        /// <summary>
        /// 終了ボタン押下イベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEnd_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }
        #endregion

    }
}
