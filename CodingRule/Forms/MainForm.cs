﻿using System;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;
using CodingRule.Util;

namespace CodingRule.Forms
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();

            // コントロール背景色の初期化
            ContorolUtil.SetContorolsColor(this);

            // アイコンをすべての画面に反映
            typeof(Form).GetField("defaultIcon", BindingFlags.NonPublic | BindingFlags.Static).SetValue(null, new Icon(@"ICO_C#.ico"));

            this.btnForm1.Focus();
        }
        /// <summary>
        /// フォーム1ボタン押下
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnForm1_Click(object sender, EventArgs e)
        {
            this.FormOpenCommon(new Form1());
        }
        /// <summary>
        /// フォーム2ボタン押下
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnForm2_Click(object sender, EventArgs e)
        {
            this.FormOpenCommon(new Form2());
        }
        /// <summary>
        /// フォーム表示処理（共通）
        /// </summary>
        /// <param name="ChildForm"></param>
        private void FormOpenCommon(Form ChildForm) {
            Application.EnableVisualStyles();
            this.Visible = false;
            ChildForm.ShowDialog();
            this.Visible = true;
        }
        /// <summary>
        /// 終了ボタン押下
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEnd_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
