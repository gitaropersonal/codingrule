﻿using CodingRule.Util;
using System;
using System.Windows.Forms;
using WK.Libraries.BetterFolderBrowserNS;

namespace CodingRule.Forms
{
    public partial class Form2 : Form
    {
        /// <summary>
        /// フォルダ選択ダイアログ表示数（最大1とする）
        /// </summary>
        private static int _DIALOG_COUNT = 0;

        public Form2()
        {
            InitializeComponent();

            // コントロール背景色の初期化
            ContorolUtil.SetContorolsColor(this);
        }
        /// <summary>
        /// フォルダ選択ボタン押下処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnFolderSelect_Click(object sender, EventArgs e)
        {
            if (0 < _DIALOG_COUNT)
            {
                // 1つでもダイアログを表示させている場合は処理しない
                this.SendToBack();
                return;
            }
            _DIALOG_COUNT++;

            var dialog = new BetterFolderBrowser()
            {
                Multiselect = false,
            };

            if (dialog.ShowDialog() != DialogResult.OK)
            {
                // キャンセルの場合はダイアログを閉じる
                _DIALOG_COUNT = 0;
                return;
            }

            // フォルダ選択された場合はフォルダのパスをテキストボックスに表示
            this.txtFolderPath.Text = dialog.SelectedFolder;
            _DIALOG_COUNT = 0;
        }
        /// <summary>
        /// 終了ボタン押下
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEnd_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
