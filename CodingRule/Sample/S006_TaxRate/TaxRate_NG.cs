﻿using CodingRule.Const;
using System;

namespace CodingRule.Sample.S006_TaxRate
{
    public class TaxRate_NG
    {
        /// <summary>
        /// 税率取得
        /// </summary>
        /// <param name="uriageDate">売上日</param>
        /// <param name="isKeigenTaisho">軽減税率対象か？</param>
        /// <returns>税率</returns>
        public decimal GetTaxRate(DateTime uriageDate, bool isKeigenTaisho)
        {
            decimal ret = decimal.Zero;

            if (uriageDate < TaxConst._TAX_START_DATE_3PCT)
            {
                // 消費税導入前なら0%
                ret = decimal.Zero;
            }
            else if (uriageDate < TaxConst._TAX_START_DATE_5PCT)
            {
                // 消費税5%導入前なら3%
                ret = TaxConst._RATE_3PCT;
            }
            else if (uriageDate < TaxConst._TAX_START_DATE_8PCT)
            {
                // 消費税8%導入前なら5%
                ret = TaxConst._RATE_5PCT;
            }
            else if (uriageDate < TaxConst._TAX_START_DATE_10PCT)
            {
                // 消費税10%導入前なら8%
                ret = TaxConst._RATE_8PCT;
            }
            else
            {
                if (isKeigenTaisho)
                {
                    // 軽減税率対象なら8%
                    ret = TaxConst._RATE_8PCT;
                }
                else
                {
                    // 軽減税率対象外なら10%
                    ret = TaxConst._RATE_10PCT;
                }
            }
            // 税率を返す
            return ret;
        }
    }
}
