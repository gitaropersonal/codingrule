﻿using CodingRule.Const;
using System;

namespace CodingRule.Sample.S006_TaxRate
{
    public class TaxRate_OK
    {

        /// <summary>
        /// 税率取得
        /// </summary>
        /// <param name="uriageDate">売上日</param>
        /// <param name="isKeigenTaisho">軽減税率対象か？</param>
        /// <returns>税率</returns>
        public decimal GetTaxRate(DateTime uriageDate, bool isKeigenTaisho)
        {
            if (uriageDate < TaxConst._TAX_START_DATE_3PCT)
            {
                // 消費税導入前なら0%
                return decimal.Zero;
            }
            if (uriageDate < TaxConst._TAX_START_DATE_5PCT)
            {
                // 消費税5%導入前なら3%
                return TaxConst._RATE_3PCT;
            }
            if (uriageDate < TaxConst._TAX_START_DATE_8PCT)
            {
                // 消費税8%導入前なら5%
                return TaxConst._RATE_5PCT;
            }
            if (uriageDate < TaxConst._TAX_START_DATE_10PCT)
            {
                // 消費税10%導入前なら8%
                return TaxConst._RATE_8PCT;
            }
            if (isKeigenTaisho)
            {
                // 軽減税率対象なら8%
                return TaxConst._RATE_8PCT;
            }
            // 軽減税率対象外なら10%
            return TaxConst._RATE_10PCT;
        }
    }
}
