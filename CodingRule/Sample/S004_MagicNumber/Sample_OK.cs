﻿using CodingRule.Const;
using System;

namespace CodingRule.Sample.S004_MagicNumber
{
    public class Sample_OK
    {
        /// <summary>
        /// サンプルメソッド1
        /// </summary>
        /// <param name="zangyoTime">残業時間</param>
        public void Sample1(decimal zangyoTime)
        {
            // 計算
            decimal zangyodai = Math.Truncate(zangyoTime * GenkaConst._WARIMASHI_RATE_ZANGYO / GenkaConst._WORK_TIME_PER_MONTH);

            // 計算結果を出力
            Console.WriteLine(zangyodai.ToString());
        }
    }
}
