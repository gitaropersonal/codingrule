﻿using System;

namespace CodingRule.Sample.S004_MagicNumber
{
    public class Sample_NG
    {

        public string str { get; set;  }


        /// <summary>
        /// サンプルメソッド1
        /// </summary>
        /// <param name="zt"></param>
        public void Sample1(decimal zt)
        {
            // 計算
            decimal k = Math.Truncate(zt * 1.28M / 152);

            // 計算結果を出力
            Console.WriteLine(k.ToString());
        }
    }
}
