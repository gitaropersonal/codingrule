﻿using CodingRule.Const;
using System;
using System.Windows.Forms;

namespace CodingRule.Sample.S003_Messages
{
    public class Screen_OK
    {
        /// <summary>
        /// データ作成処理
        /// </summary>
        public void DataCreate()
        {
            // データ作成フラグ
            bool isDataCreated = false;

            try
            {
                #region 処理
                #endregion

                if (isDataCreated)
                {
                    // 作成成功
                    MessageBox.Show(Messages._MSG_DATA_CREATED, Messages._CAP_INFO);
                    return;
                }
                // 作成失敗
                MessageBox.Show(Messages._MSG_DATA_FAILED, Messages._CAP_CAUTION);

            }
            catch (Exception ex)
            {

                // エラー発生
                string msg = string.Format(Messages._MSG_ERROR, ex.Message);
                MessageBox.Show(msg, Messages._CAP_ERROR);
            }
        }
    }
}
