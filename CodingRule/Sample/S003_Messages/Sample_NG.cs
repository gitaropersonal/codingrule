﻿using System;
using System.Windows.Forms;

namespace CodingRule.Sample.S003_Messages
{
    public class Screen_NG
    {
        /// <summary>
        /// データ作成処理
        /// </summary>
        public void DataCreate()
        {
            // データ作成フラグ
            bool isDataCreated = false;

            try
            {
                #region 処理
                #endregion

                if (isDataCreated)
                {
                    // 作成成功
                    MessageBox.Show("データを作成しました。", "確認");
                    return;
                }
                // 作成失敗
                MessageBox.Show("データ作成に失敗しました。", "警告");

            }
            catch (Exception ex)
            {
                // エラー発生
                string msg = "エラーが発生しました。\r\n" + ex.Message;
                MessageBox.Show(msg, "エラー");
            }
        }
    }
}
