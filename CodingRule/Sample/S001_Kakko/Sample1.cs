﻿/// <summary>
/// 名前空間
/// </summary>
namespace CodingRule.Sample.S001_Kakko
{
    /// <summary>
    /// クラス
    /// </summary>
    class Sample1
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        private Sample1()
        {
        }
        /// <summary>
        /// サンプルメソッド1
        /// </summary>
        private void Method1()
        {
            // if-else文
            if (true)
            {

            }
            else
            {

            }
            // for文
            for (int i = 0; i < 10; i++)
            {

            }
            // try-catch-finally
            try
            {

            }
            catch
            {

            }
            finally
            {

            }
        }
    }
}
