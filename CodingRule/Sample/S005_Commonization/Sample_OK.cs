﻿namespace CodingRule.Sample.S005_Commonization {
    class Sample_OK
    {

        /// <summary>
        /// サンプルメソッド1
        /// </summary>
        /// <param name="arg1"></param>
        /// <param name="arg2"></param>
        /// <param name="arg3"></param>
        /// <param name="arg4"></param>
        /// <returns></returns>
        private string[] Method1(string arg1, string arg2, string arg3, string arg4)
        {
            arg1 = EditNullOrEmpty(arg1);
            arg2 = EditNullOrEmpty(arg2);
            arg3 = EditNullOrEmpty(arg3);
            arg4 = EditNullOrEmpty(arg4);
            return new string[] { arg1, arg2, arg3, arg4 };
        }

        /// <summary>
        /// nullかスペースの場合は空文字を返す
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private string EditNullOrEmpty(string arg)
        {
            if (string.IsNullOrWhiteSpace(arg))
            {
                arg = string.Empty;
            }
            return arg;
        }

    }
}
