﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingRule.Sample.S005_Commonization
{
    class Sample_NG
    {
        /// <summary>
        /// サンプルメソッド1
        /// </summary>
        /// <param name="arg1"></param>
        /// <param name="arg2"></param>
        /// <param name="arg3"></param>
        /// <param name="arg4"></param>
        /// <returns></returns>
        private string[] Method1(string arg1, string arg2, string arg3, string arg4)
        {
            if (string.IsNullOrWhiteSpace(arg1))
            {
                arg1 = string.Empty;
            }
            if (string.IsNullOrWhiteSpace(arg2))
            {
                arg2 = string.Empty;
            }
            if (string.IsNullOrWhiteSpace(arg3))
            {
                arg3 = string.Empty;
            }
            if (string.IsNullOrWhiteSpace(arg4))
            {
                arg4 = string.Empty;
            }
            return new string[] { arg1, arg2, arg3, arg4 };
        }
    }
}
