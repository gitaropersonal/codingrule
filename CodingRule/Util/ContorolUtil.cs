﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace CodingRule.Util
{
    public static class ContorolUtil
    {
        /// <summary>
        /// コントロール背景色の初期化
        /// </summary>
        /// <param name="_Body"></param>
        public static void SetContorolsColor(Form _Body) {

            // フォーム背景色
            _Body.BackColor = SystemColors.GradientInactiveCaption;

            // 全コントロール取得
            List<Control> controls = GetAllControls<Control>(_Body);
            foreach (var control in controls)
            {
                if (control.GetType() == typeof(Button))
                {
                    // ボタン背景色セット
                    Button button = (Button)control;
                    button.BackColor = SystemColors.MenuHighlight;
                    button.ForeColor = Color.White;
                    continue;
                }
                if (control.GetType() == typeof(TextBox))
                {
                    // テキストボックス背景色セット
                    TextBox txtBox = (TextBox)control;
                    txtBox.BackColor = Color.Empty;
                    txtBox.ForeColor = SystemColors.ControlText;
                    continue;
                }
                if (control.GetType() == typeof(ComboBox))
                {
                    // コンボボックス背景色セット
                    ComboBox cmbBox = (ComboBox)control;
                    cmbBox.BackColor = Color.Empty;
                    cmbBox.ForeColor = SystemColors.ControlText;
                    continue;
                }
            }
        }
        /// <summary>
        /// 全コントロール取得
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="top"></param>
        /// <returns></returns>
        public static List<T> GetAllControls<T>(Control top) where T : Control
        {
            List<T> buf = new List<T>();
            foreach (Control ctrl in top.Controls)
            {
                if (ctrl is T) buf.Add((T)ctrl);
                buf.AddRange(GetAllControls<T>(ctrl));
            }
            return buf;
        }
    }
}
