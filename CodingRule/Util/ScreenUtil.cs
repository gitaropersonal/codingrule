﻿using CodingRule.Const;
using System.Linq;

namespace CodingRule.Util {
    public static class ScreenUtil {
        /// <summary>
        /// 画面名取得
        /// </summary>
        /// <param name="screenId">画面ID</param>
        /// <returns></returns>
        public static string GetScreenName(int screenId) {
            if (ScreenConst._DIC_SCREEN.ContainsKey(screenId)) {
                return ScreenConst._DIC_SCREEN.Where(n => n.Key == screenId).First().Value;
            }
            return string.Empty;
        }
    }
}
