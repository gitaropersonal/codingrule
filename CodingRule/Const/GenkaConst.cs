﻿namespace CodingRule.Const {
    public static class GenkaConst {
        /// <summary>
        /// 残業代計算要定数
        /// </summary>
        public const decimal _WARIMASHI_RATE_ZANGYO = 1.28M;
        public const int _WORK_TIME_PER_MONTH = 152;
    }
}
