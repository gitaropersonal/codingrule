﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingRule.Const
{
    public static class Messages
    {
        // キャプション
        public const string _CAP_INFO = "確認";
        public const string _CAP_CAUTION = "警告";
        public const string _CAP_ERROR = "エラー";

        // メッセージ
        public const string _MSG_DATA_CREATED = "データを作成しました。";
        public const string _MSG_DATA_FAILED = "データ作成に失敗しました。";
        public const string _MSG_ERROR = "エラーが発生しました。\r\n{0}";
        public const string _MSG_OVER_MAX_BYTE = "{0}には{1}Byteまでの文字しか入力できません。";
    }
}
