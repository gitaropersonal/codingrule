﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingRule.Const
{
    public static class ScreenConst
    {
        // 画面ID
        public const int _SCREEN_ID_00 = 0;
        public const int _SCREEN_ID_10 = 10;
        public const int _SCREEN_ID_20 = 20;
        public const int _SCREEN_ID_30 = 30;
        public const int _SCREEN_ID_40 = 40;

        // 画面名
        public const string _SCREEN_NAME_00 = "メニュー画面";
        public const string _SCREEN_NAME_10 = "データ入力画面";
        public const string _SCREEN_NAME_20 = "データ照会画面";
        public const string _SCREEN_NAME_30 = "マスタメンテナンス画面";
        public const string _SCREEN_NAME_40 = "月次処理画面";

        // 画面ID・名称Dictionary
        public static Dictionary<int, string> _DIC_SCREEN = new Dictionary<int, string> {
            { _SCREEN_ID_00, _SCREEN_NAME_00},
            { _SCREEN_ID_10, _SCREEN_NAME_10},
            { _SCREEN_ID_20, _SCREEN_NAME_20},
            { _SCREEN_ID_30, _SCREEN_NAME_30},
            { _SCREEN_ID_40, _SCREEN_NAME_40},
        };

    }
}
