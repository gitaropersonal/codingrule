﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingRule.Const {
    public static class TaxConst {
        // 消費税施行日
        public static DateTime _TAX_START_DATE_3PCT = new DateTime(1989, 4, 1);
        public static DateTime _TAX_START_DATE_5PCT = new DateTime(1997, 4, 1);
        public static DateTime _TAX_START_DATE_8PCT = new DateTime(2014, 4, 1);
        public static DateTime _TAX_START_DATE_10PCT = new DateTime(2019, 10, 1);

        // 税率
        public const decimal _RATE_3PCT = 0.03M;
        public const decimal _RATE_5PCT = 0.05M;
        public const decimal _RATE_8PCT = 0.08M;
        public const decimal _RATE_10PCT = 0.010M;
    }
}
